"
Like & dislikes de cuoora.
"
Class {
	#name : #Vote,
	#superclass : #Object,
	#instVars : [
		'isLike',
		'propietario',
		'fechaCreacion'
	],
	#category : #Cuoora
}

{ #category : #initialization }
Vote class >> dislike [ 
 ^ self new init: false.
]

{ #category : #initialization }
Vote class >> like [ 
 ^ self new init: true
]
