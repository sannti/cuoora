"
Publicaciones de cuoora.
"
Class {
	#name : #Publicacion,
	#superclass : #Object,
	#instVars : [
		'texto',
		'usuario',
		'fechaCreacion'
	],
	#category : #Cuoora
}

{ #category : #'as yet unclassified' }
Publicacion >> esDueño: unUsuario [
	^ usuario = unUsuario
]
