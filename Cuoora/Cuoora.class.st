"
cuOOra app
"
Class {
	#name : #Cuoora,
	#superclass : #Object,
	#instVars : [
		'usuarios',
		'topicos',
		'preguntas'
	],
	#category : #Cuoora
}

{ #category : #'private-initialization' }
Cuoora class >> initialize [
	^ self new initialize
]

{ #category : #agregation }
Cuoora >> agregarPregunta: unaPregunta [
	preguntas add: unaPregunta.
]

{ #category : #agregation }
Cuoora >> agregarTopico: unTopico [
	topicos add: unTopico.
]

{ #category : #agregation }
Cuoora >> agregarUsuario: unUsuario [
	usuarios add: unUsuario.
]

{ #category : #getters }
Cuoora >> getPreguntasTopico: unTopico [
	^ preguntas detect: [ :preg | preg incluyeTopico: unTopico ] ifNone: [ nil ]
]

{ #category : #getters }
Cuoora >> getRespuestasUsuario: unUsuario [
	^ preguntas
		collect:
			[ :preg | preg respuestas select: [ :resp | resp esDueño: unUsuario ] ]
]

{ #category : #initialization }
Cuoora >> initialize [
	usuarios:= Set new.
	topicos:= Set new.
	preguntas:= OrderedCollection new.
]
