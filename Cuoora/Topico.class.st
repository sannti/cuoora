"
Topicos de cuoora
"
Class {
	#name : #Topico,
	#superclass : #Object,
	#instVars : [
		'nombre',
		'descripcion',
		'fechaCreacion'
	],
	#category : #Cuoora
}

{ #category : #initialization }
Topico class >> nombre: unNombre descripcion: unaDescripcion [
	^ self new nombre: unNombre descripcion: unaDescripcion
]

{ #category : #initialization }
Topico >> descripcion [
	^ descripcion 
]

{ #category : #initialization }
Topico >> existTopicName: unTopico [
	^ nombre = unTopico nombre 
]

{ #category : #initialization }
Topico >> nombre [
	^ nombre
]

{ #category : #initialization }
Topico >> nombre: unNombre descripcion: unaDescripcion [
	nombre := unNombre.
	descripcion := unaDescripcion.
	fechaCreacion := Date today
]
