"
Usuarios de cuoora
"
Class {
	#name : #Usuario,
	#superclass : #Object,
	#instVars : [
		'username',
		'password',
		'following',
		'preguntas',
		'topicos',
		'fechaCreacion'
	],
	#category : #Cuoora
}

{ #category : #accessing }
Usuario >> follow: aUser [
	following add: aUser
]

{ #category : #accessing }
Usuario >> following [
	^ following 
]

{ #category : #accessing }
Usuario >> getPreguntasRelevantes [
	| pregs |
	pregs := Set new.
	pregs
		addAll:
			(topicos collect: [ :top | preguntas incluyeTopico: top ]).
	pregs
		addAll:
			(following
				collect: [ :user | user preguntas ]).
	^ pregs
]

{ #category : #accessing }
Usuario >> getPuntajeUsuario: respuestasUsuario [
	| ptsPregs ptsRtas |
	ptsRtas := respuestasUsuario
		inject: 0
		into: [ :ptos :rtas | ptos + 50 + rtas getPuntaje ].
	ptsPregs := preguntas
		inject: 0
		into: [ :ptos :pregs | ptos + 30 + pregs getPuntaje ].
	^ ptsRtas + ptsPregs
]

{ #category : #accessing }
Usuario >> preguntas [
	^ preguntas 
]

{ #category : #accessing }
Usuario >> topicos [
	^ topicos
]
