"
Preguntas de cuoora
"
Class {
	#name : #Pregunta,
	#superclass : #Publicacion,
	#instVars : [
		'titulo',
		'topicos',
		'respuestas'
	],
	#category : #Cuoora
}

{ #category : #'as yet unclassified' }
Pregunta >> incluyeTopico: unTopico [
	^ topicos includes: unTopico
]

{ #category : #'as yet unclassified' }
Pregunta >> respuestas [
	^ respuestas 
]
