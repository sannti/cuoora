"
Respuestas de cuoora
"
Class {
	#name : #Respuesta,
	#superclass : #Publicacion,
	#instVars : [
		'pregunta'
	],
	#category : #Cuoora
}

{ #category : #initialization }
Respuesta class >> responder: unTexto pregunta: unaPregunta usuario: unUsuario [
	^ self new responder: unTexto pregunta: unaPregunta usuario: unUsuario
]

{ #category : #initialization }
Respuesta >> responder: unTexto pregunta: unaPregunta usuario: unUsuario [
	texto := unTexto.
	pregunta:= unaPregunta.
	fechaCreacion := Date today.
	usuario := unUsuario.	
]
