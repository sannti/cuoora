Class {
	#name : #'Test_Topico',
	#superclass : #TestCase,
	#category : #Cuoora
}

{ #category : #tests }
Test_Topico >> testTopic [
	| topico |
	topico:= Topico nombre: 'Prueba' descripcion: 'Topico de prueba'.
	self assert: topico nombre equals: 'Prueba'.
	self assert: topico descripcion equals: 'Topico de prueba'
]
